# Laser-Pong

The Laser-Pong game is an example of a software that is sending simply structured laser data to LaProMo (the Laser Projection Module). 
Two players are able to play the classical Pong game, which will be displayed in laser graphic projection.

Use Laser-Pong to check if your setup with LPM and IDN-Toolbox is working! Download one of the JAR files.

It will require an installation of a Java Runtime Environment (JRE) and is running on any operating system. It needs to execute on the same
computer where LaProMo is running.

**Update 26.1.2020** New LaserPongServer_v2020-v1.jar starts the game right away at startup. You can watch the game play without connected controllers.
With controllers connected, both players need to lower the paddles to the bottom most position to start the game. The game will pause after one of 
the players scores to 9 points.

## Open issues:

To play the game, two players can control the paddles each from an Android based smartphone. An Android installer APK is available from [webpage Laser-Pong Project](https://net.cs.uni-bonn.de/wg/cs/teaching-areas/labs-and-project-groups/laser-light-lab/laser-pong-project/).

**2-do:** The LaserPongController APK needs to be re-compiled for recent Android versions. The current version only works for Android 7.x and earlier.
